; KISSlicer - FREE
; Linux
; version 1.1.0.14
; Built: May  8 2013, 12:05:59
; Running on 4 cores
;
; Saved: Mon Feb 29 12:21:36 2016
; 'socket.gcode'
;
; *** Printer Settings ***
;
; printer_name = atom
; bed_STL_filename = 
; extension = gcode
; cost_per_hour = 0
; g_code_prefix = 4732380A
; g_code_warm = 3B2053656C6563742065787472756465722C207761726D
;     2C2070757267650A0A3B204266422D7374796C650A4D3C4558542B31
;     3E303420533C54454D503E0A4D3534320A4D35353C4558542B313E20
;     50333230303020533930300A4D3534330A0A3B2035442D7374796C65
;     0A543C4558542B303E0A4D31303920533C54454D503E0A
; g_code_cool = 3B2047756172616E746565642073616D65206578747275
;     6465722C20636F6F6C696E6720646F776E0A0A3B204266422D737479
;     6C650A4D3C4558542B313E303420533C54454D503E0A0A3B2035442D
;     7374796C650A4D31303420533C54454D503E0A
; g_code_N_layers = 3B204D617962652072652D686F6D65205820262059
;     3F
; g_code_postfix = 4D3134302053300A47312046333030302058300A
; post_process = NULL
; every_N_layers = 0
; num_extruders = 1
; firmware_type = 2
; add_comments = 1
; fan_on = M106
; fan_off = M107
; fan_pwm = 1
; add_m101_g10 = 0
; z_speed_mm_per_s = 3.5
; z_settle_mm = 0
; bed_size_x_mm = 140
; bed_size_y_mm = 140
; bed_size_z_mm = 120
; bed_offset_x_mm = 66
; bed_offset_y_mm = 66
; bed_offset_z_mm = 0
; bed_roughness_mm = 0.125
; travel_speed_mm_per_s = 250
; first_layer_speed_mm_per_s = 10
; dmax_per_layer_mm_per_s = 50
; xy_accel_mm_per_s_per_s = 1500
; lo_speed_perim_mm_per_s = 5
; lo_speed_solid_mm_per_s = 8
; lo_speed_sparse_mm_per_s = 8
; hi_speed_perim_mm_per_s = 40
; hi_speed_solid_mm_per_s = 45
; hi_speed_sparse_mm_per_s = 45
; ext_gain_1 = 1
; ext_material_1 = 1
; ext_axis_1 = 0
; ext_gain_2 = 1
; ext_material_2 = 0
; ext_axis_2 = 0
; ext_gain_3 = 1
; ext_material_3 = 0
; ext_axis_3 = 0
; model_ext = 0
; support_ext = 0
; support_body_ext = 0
; raft_ext = 0
; solid_loop_overlap_fraction = 0.6
;
; *** Material Settings for Extruder 1 ***
;
; material_name = PLA
; g_code_matl = 3B204D617962652073657420736F6D65206D6174657269
;     616C2D737065636966696320472D636F64653F
; fan_Z_mm = 0
; fan_loops_percent = 100
; fan_inside_percent = 0
; fan_cool_percent = 100
; temperature_C = 210
; keep_warm_C = 180
; first_layer_C = 210
; bed_C = 60
; sec_per_C_per_C = 0
; flow_min_mm3_per_s = 0.01
; flow_max_mm3_per_s = 10
; destring_suck = 2.5
; destring_prime = 2.5
; destring_min_mm = 1
; destring_trigger_mm = 100
; destring_speed_mm_per_s = 15
; Z_lift_mm = 0.1
; min_layer_time_s = 10
; wipe_mm = 10
; cost_per_cm3 = 0
; flowrate_tweak = 1
; fiber_dia_mm = 1.75
; color = 0
;
; *** Style Settings ***
;
; style_name = sample style
; layer_thickness_mm = 0.25
; extrusion_width_mm = 0.4
; num_loops = 3
; skin_thickness_mm = 1
; infill_extrusion_width = 0.4
; infill_density_denominator = 7
; stacked_layers = 1
; use_destring = 1
; use_wipe = 1
; loops_insideout = 1
; infill_st_oct_rnd = 0
; inset_surface_xy_mm = 0
; seam_jitter_degrees = 0
; seam_depth_scaler = 1
;
; *** Support Settings ***
;
; support_name = sample support
; support_sheathe = 0
; support_density = 0
; support_inflate_mm = 0
; support_gap_mm = 0.3
; support_angle_deg = 60
; support_z_max_mm = -1
; sheathe_z_max_mm = -1
; raft_mode = 0
; prime_pillar_mode = 0
; raft_inflate_mm = 3
;
; *** Actual Slicing Settings As Used ***
;
; layer_thickness_mm = 0.25
; extrusion_width = 0.4
; num_ISOs = 3
; wall_thickness = 1
; infill_style = 10
; support_style = 0
; support_angle = 59.9
; destring_min_mm = 1
; stacked_infill_layers = 1
; raft_style = 0
; extra_raft_depth = 0.125
; oversample_res_mm = 0.125
; crowning_threshold_mm = 1
; loops_insideout = 1
; solid_loop_overlap_fraction = 0.6
; inflate_raft_mm = 0
; inflate_support_mm = 0
; model_support_gap_mm = 0.3
; infill_st_oct_rnd = 0
; support_Z_max_mm = 1e+20
; sheathe_Z_max_mm = 0
; inset_surface_xy_mm = 0
; seam_jitter_degrees = 0
; seam_depth_scaler = 1
; Speed vs Quality = 0.30
; Perimeter Speed = 29.50
; Solid Speed = 33.90
; Sparse Speed = 33.90
;
; *** G-code Prefix ***
;
G28

;
; *** Main G-code ***
;
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=0.38
;
; *** Warming Extruder 1 to 210 C ***
; Select extruder, warm, purge
; BfB-style
M104 S210
M542
M551 P32000 S900
M543
; 5D-style
T0
M109 S210

;
G92 E0
;
; *** Slowing to match Min Layer Time (speed multiplier is 0.922662) ***
; fan on
M106
;
; 'Loop', 0.6 [feed mm/s], 9.2 [head mm/s]
G1 X66.23 Y71.33 Z0.475 E0 F15000
G1 X66.23 Y71.33 Z0.375 E0 F210
G1 E2.5 F900
G1 X65.75 Y71.33 E2.5299 F553.6
G1 X65.75 Y63.49 E3.0185
G1 X65.76 Y60.67 E3.1942
G1 X66.25 Y60.67 E3.2247
G1 X66.25 Y69.26 E3.7604
G1 X66.23 Y71.33 E3.889
;
; 'Loop', 0.6 [feed mm/s], 9.2 [head mm/s]
G1 X66.63 Y71.73 E3.889 F15000
G1 X65.36 Y71.73 E3.9687 F553.6
G1 X65.35 Y70.61 E4.0386
G1 X65.35 Y60.27 E4.6829
G1 X66.65 Y60.27 E4.7635
G1 X66.65 Y67.43 E5.2096
G1 X66.63 Y71.73 E5.4777
;
; 'Perimeter', 0.6 [feed mm/s], 9.2 [head mm/s]
G1 X66.91 Y71.88 E5.4777 F15000
G1 X67.04 Y72.13 E5.495 F553.6
G1 X64.96 Y72.13 E5.6243
G1 X64.95 Y71.95 E5.6352
G1 X64.95 Y60.05 E6.3776
G1 X64.96 Y59.87 E6.3886
G1 X67.04 Y59.87 E6.5178
G1 X67.05 Y60.05 E6.5288
G1 X67.05 Y71.95 E7.2712
G1 X67.04 Y72.13 E7.2822
G1 X66.8 Y71.98 E7.2995
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 9.2 [head mm/s]
G1 E4.7995 F900
G1 X66.91 Y71.88 E4.7995 F553.6
G1 X67.04 Y72.13 E4.7995
G1 X64.96 Y72.13 E4.7995
G1 X64.95 Y71.95 E4.7995
G1 X64.95 Y64.48 E4.7995
G1 X64.95 Y64.48 Z0.725 E4.7995 F210
; END_LAYER_OBJECT z=0.38
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=0.62
; *** Slowing to match Min Layer Time (speed multiplier is 0.342749) ***
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.23 Y71.33 Z0.725 E0 F15000
G1 X66.23 Y71.33 Z0.625 E0 F210
G1 E2.5 F900
G1 X65.75 Y71.33 E2.52 F697.2
G1 X65.75 Y63.49 E2.8457
G1 X65.76 Y60.67 E2.9628
G1 X66.25 Y60.67 E2.9832
G1 X66.25 Y69.26 E3.3403
G1 X66.23 Y71.33 E3.426
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.63 Y71.73 E3.426 F15000
G1 X65.36 Y71.73 E3.4792 F697.2
G1 X65.35 Y70.61 E3.5257
G1 X65.35 Y60.27 E3.9553
G1 X66.65 Y60.27 E4.009
G1 X66.65 Y67.43 E4.3064
G1 X66.63 Y71.73 E4.4851
;
; 'Perimeter', 0.4 [feed mm/s], 10.1 [head mm/s]
G1 X66.91 Y71.88 E4.4851 F15000
G1 X67.04 Y72.13 E4.4967 F606.7
G1 X64.96 Y72.13 E4.5828
G1 X64.95 Y71.95 E4.5902
G1 X64.95 Y60.05 E5.0851
G1 X64.96 Y59.87 E5.0924
G1 X67.04 Y59.87 E5.1786
G1 X67.05 Y60.05 E5.1859
G1 X67.05 Y71.95 E5.6808
G1 X67.04 Y72.13 E5.6881
G1 X66.8 Y71.98 E5.6997
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 11.6 [head mm/s]
G1 E3.1997 F900
G1 X66.91 Y71.88 E3.1997 F697.2
G1 X67.04 Y72.13 E3.1997
G1 X64.96 Y72.13 E3.1997
G1 X64.95 Y71.95 E3.1997
G1 X64.95 Y64.48 E3.1997
G1 X64.95 Y64.48 Z0.975 E3.1997 F210
; END_LAYER_OBJECT z=0.62
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=0.88
; *** Slowing to match Min Layer Time (speed multiplier is 0.342749) ***
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.23 Y71.33 Z0.975 E0 F15000
G1 X66.23 Y71.33 Z0.875 E0 F210
G1 E2.5 F900
G1 X65.75 Y71.33 E2.52 F697.2
G1 X65.75 Y63.49 E2.8457
G1 X65.76 Y60.67 E2.9628
G1 X66.25 Y60.67 E2.9832
G1 X66.25 Y69.26 E3.3403
G1 X66.23 Y71.33 E3.426
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.63 Y71.73 E3.426 F15000
G1 X65.36 Y71.73 E3.4792 F697.2
G1 X65.35 Y70.61 E3.5257
G1 X65.35 Y60.27 E3.9553
G1 X66.65 Y60.27 E4.009
G1 X66.65 Y67.43 E4.3064
G1 X66.63 Y71.73 E4.4851
;
; 'Perimeter', 0.4 [feed mm/s], 10.1 [head mm/s]
G1 X66.91 Y71.88 E4.4851 F15000
G1 X67.04 Y72.13 E4.4967 F606.7
G1 X64.96 Y72.13 E4.5828
G1 X64.95 Y71.95 E4.5902
G1 X64.95 Y60.05 E5.0851
G1 X64.96 Y59.87 E5.0924
G1 X67.04 Y59.87 E5.1786
G1 X67.05 Y60.05 E5.1859
G1 X67.05 Y71.95 E5.6808
G1 X67.04 Y72.13 E5.6881
G1 X66.8 Y71.98 E5.6997
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 11.6 [head mm/s]
G1 E3.1997 F900
G1 X66.91 Y71.88 E3.1997 F697.2
G1 X67.04 Y72.13 E3.1997
G1 X64.96 Y72.13 E3.1997
G1 X64.95 Y71.95 E3.1997
G1 X64.95 Y64.48 E3.1997
G1 X64.95 Y64.48 Z1.225 E3.1997 F210
; END_LAYER_OBJECT z=0.88
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=1.12
; *** Slowing to match Min Layer Time (speed multiplier is 0.342749) ***
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.23 Y71.33 Z1.225 E0 F15000
G1 X66.23 Y71.33 Z1.125 E0 F210
G1 E2.5 F900
G1 X65.75 Y71.33 E2.52 F697.2
G1 X65.75 Y63.49 E2.8457
G1 X65.76 Y60.67 E2.9628
G1 X66.25 Y60.67 E2.9832
G1 X66.25 Y69.26 E3.3403
G1 X66.23 Y71.33 E3.426
;
; 'Loop', 0.5 [feed mm/s], 11.6 [head mm/s]
G1 X66.63 Y71.73 E3.426 F15000
G1 X65.36 Y71.73 E3.4792 F697.2
G1 X65.35 Y70.61 E3.5257
G1 X65.35 Y60.27 E3.9553
G1 X66.65 Y60.27 E4.009
G1 X66.65 Y67.43 E4.3064
G1 X66.63 Y71.73 E4.4851
;
; 'Perimeter', 0.4 [feed mm/s], 10.1 [head mm/s]
G1 X66.91 Y71.88 E4.4851 F15000
G1 X67.04 Y72.13 E4.4967 F606.7
G1 X64.96 Y72.13 E4.5828
G1 X64.95 Y71.95 E4.5902
G1 X64.95 Y60.05 E5.0851
G1 X64.96 Y59.87 E5.0924
G1 X67.04 Y59.87 E5.1786
G1 X67.05 Y60.05 E5.1859
G1 X67.05 Y71.95 E5.6808
G1 X67.04 Y72.13 E5.6881
G1 X66.8 Y71.98 E5.6997
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 11.6 [head mm/s]
G1 E3.1997 F900
G1 X66.91 Y71.88 E3.1997 F697.2
G1 X67.04 Y72.13 E3.1997
G1 X64.96 Y72.13 E3.1997
G1 X64.95 Y71.95 E3.1997
G1 X64.95 Y64.48 E3.1997
G1 X64.95 Y64.48 Z1.475 E3.1997 F210
; END_LAYER_OBJECT z=1.12
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=1.38
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z1.475 E0 F15000
G1 X66.85 Y71.23 Z1.375 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z1.725 E2.3728 F210
; END_LAYER_OBJECT z=1.38
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=1.62
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z1.725 E0 F15000
G1 X66.85 Y71.23 Z1.625 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z1.975 E2.3728 F210
; END_LAYER_OBJECT z=1.62
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=1.88
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z1.975 E0 F15000
G1 X66.85 Y71.23 Z1.875 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z2.225 E2.3728 F210
; END_LAYER_OBJECT z=1.88
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=2.12
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z2.225 E0 F15000
G1 X66.85 Y71.23 Z2.125 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z2.475 E2.3728 F210
; END_LAYER_OBJECT z=2.12
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=2.38
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z2.475 E0 F15000
G1 X66.85 Y71.23 Z2.375 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z2.725 E2.3728 F210
; END_LAYER_OBJECT z=2.38
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=2.62
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z2.725 E0 F15000
G1 X66.85 Y71.23 Z2.625 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z2.975 E2.3728 F210
; END_LAYER_OBJECT z=2.62
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=2.88
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z2.975 E0 F15000
G1 X66.85 Y71.23 Z2.875 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z3.225 E2.3728 F210
; END_LAYER_OBJECT z=2.88
; Reset extruder pos
G92 E0
; BEGIN_LAYER_OBJECT z=3.12
; *** Slowing to match Min Layer Time (speed multiplier is 0.308922) ***
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.85 Y71.23 Z3.225 E0 F15000
G1 X66.85 Y71.23 Z3.125 E0 F210
G1 E2.5 F900
G1 X66.66 Y71.18 E2.508 F546.8
G1 X66.4 Y71.3 E2.5197
G1 X65.6 Y71.3 E2.5532
G1 X65.34 Y71.19 E2.5648
G1 X65.28 Y71.06 E2.5709
G1 X65.28 Y60.94 E2.9912
G1 X65.34 Y60.81 E2.9973
G1 X65.6 Y60.7 E3.009
G1 X66.4 Y60.7 E3.0423
G1 X66.66 Y60.81 E3.054
G1 X66.72 Y60.94 E3.06
G1 X66.72 Y71.06 E3.4804
G1 X66.66 Y71.18 E3.4863
G1 X66.71 Y71.37 E3.4943
;
; 'Perimeter', 0.4 [feed mm/s], 9.1 [head mm/s]
G1 X66.91 Y71.88 E3.4943 F15000
G1 X67.04 Y72.13 E3.5058 F546.8
G1 X64.96 Y72.13 E3.592
G1 X64.95 Y71.95 E3.5993
G1 X64.95 Y60.05 E4.0942
G1 X64.96 Y59.87 E4.1016
G1 X67.04 Y59.87 E4.1877
G1 X67.05 Y60.05 E4.195
G1 X67.05 Y71.95 E4.69
G1 X67.04 Y72.13 E4.6973
G1 X66.8 Y71.98 E4.7088
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X65.24 Y71.48 E4.7088 F15000
G1 X65.32 Y71.59 E4.7142 F593.4
G1 X65.34 Y71.75 E4.7207
G1 X65.4 Y71.7 E4.7238
G1 X65.53 Y71.72 E4.7292
G1 X66.65 Y71.73 E4.7736
G1 X66.6 Y71.7 E4.7759
G1 X66.76 Y71.48 E4.7866
;
; 'Crown', 0.4 [feed mm/s], 9.9 [head mm/s]
G1 X66.76 Y60.52 E4.7866 F15000
G1 X66.68 Y60.41 E4.792 F593.4
G1 X66.66 Y60.25 E4.7985
G1 X66.62 Y60.29 E4.8006
G1 X65.56 Y60.28 E4.8429
G1 X65.24 Y60.52 E4.859
G1 X65.4 Y60.3 E4.8697
G1 X65.34 Y60.25 E4.8728
;
; 'Wipe (and De-string)', 0.0 [feed mm/s], 10.5 [head mm/s]
G1 E2.3728 F900
G1 X65.4 Y60.3 E2.3728 F628.3
G1 X65.24 Y60.52 E2.3728
G1 X65.56 Y60.28 E2.3728
G1 X66.62 Y60.29 E2.3728
G1 X66.66 Y60.25 E2.3728
G1 X66.68 Y60.41 E2.3728
G1 X66.76 Y60.52 E2.3728
G1 X66.76 Y60.52 Z3.725 E2.3728 F210
; END_LAYER_OBJECT z=3.12
;
; *** Cooling Extruder 1 to 0 C ***
; Guaranteed same extruder, cooling down
; BfB-style
M104 S0
; 5D-style
M104 S0

;
; fan off
M107
; *** G-code Postfix ***
;
M140 S0
G1 F3000 X0

;
;
;
; Estimated Build Time:   2.01 minutes
; Estimated Build Volume: 0.080 cm^3
; Estimated Build Cost:   $0.00
;
; *** Extrusion Time Breakdown ***
; * estimated time in [s]
; * before possibly slowing down for 'cool'
; * not including Z-travel
;	+-------------+-------------+-------------+-----------------------+
;	| Extruder #1 | Extruder #2 | Extruder #3 | Path Type             |
;	+-------------+-------------+-------------+-----------------------+
;	| 2.47365     | 0           | 0           | Move                  |
;	| 0           | 0           | 0           | Pillar                |
;	| 0           | 0           | 0           | Raft                  |
;	| 0           > 0           > 0           > Support Interface     |
;	| 0           | 0           | 0           | Support (may Stack)   |
;	| 23.5273     | 0           | 0           | Perimeter             |
;	| 9.5903      | 0           | 0           | Loop                  |
;	| 0           > 0           > 0           > Solid                 |
;	| 0           | 0           | 0           | Sparse Infill         |
;	| 0           | 0           | 0           | Stacked Sparse Infill |
;	| 7.61737     | 0           | 0           | Wipe (and De-string)  |
;	| 2.6753      > 0           > 0           > Crown                 |
;	| 0           | 0           | 0           | Prime Pillar          |
;	| 0           | 0           | 0           | Skirt                 |
;	| 0           | 0           | 0           | Pause                 |
;	| 0           > 0           > 0           > Extruder Warm-Up      |
;	+-------------+-------------+-------------+-----------------------+
; Total estimated (pre-cool) minutes: 0.76
