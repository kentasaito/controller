#include "Mouse.h"

#define LOWER 300
#define UPPER 450
#define GAP 50

#define SENSOR A11
#define DRAG 11
#define SPEED A6
#define ANGLE A7
#define WARNING 5

void setup() {
  Serial.begin(115200);
  pinMode(WARNING, OUTPUT);
  pinMode(DRAG, INPUT_PULLUP);
  Mouse.begin();
}

void loop() {
  static int lastButton = 0;
  static int lastSensor = 0;
  int button;
  int raw;
  int sensor;
  int delta;

  button = digitalRead(DRAG);
  if (button != lastButton)
  {
    button ? Mouse.release() : Mouse.press();
    lastButton = button;
  }

  raw = analogRead(SENSOR);
  debug(raw);
  digitalWrite(WARNING, raw < LOWER - GAP || raw > UPPER + GAP);

  sensor = max(LOWER, min(UPPER, raw));
  delta = sensor - lastSensor;
  mouseMove(delta);
  lastSensor = sensor;
}

void mouseMove(int delta)
{
  double radius = analogRead(SPEED) / 64.0 * delta;
  double theta = (analogRead(ANGLE) -256.0) * PI / 512.0;
  int dx = radius * cos(theta);
  int dy = radius * sin(theta);
  Mouse.move(dx, dy);
}

void debug(int raw)
{
  Serial.print(" Drag = ");
  Serial.print(digitalRead(DRAG));
  Serial.print(" Speed = ");
  Serial.print(analogRead(SPEED));
  Serial.print(" Angle = ");
  Serial.print(analogRead(ANGLE));

  Serial.print(" Raw = ");
  Serial.print(raw);
  Serial.print(" Voltage = ");
  Serial.print(raw / 204.8);
  Serial.print("V");

  if (raw < LOWER)
  {
    Serial.print(" LOW");
  }
  if (raw > UPPER)
  {
    Serial.print(" HIGH");
  }
  
  Serial.println("");
}

