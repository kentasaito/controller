// ピンアサイン
const int LED = 13;
const int ENC_A = 22;
const int ENC_B = 21;

// テーブル
int spd_t[16];
float cos_t[16];
float sin_t[16];

// チャタリング回避時間(マイクロ秒)
const int DELAY = 500;

void setup()
{
  int i;

  for (i = 0; i < 16; i++)
  {
    spd_t[i] = i * 8;
    spd_t[i] = max(spd_t[i], 1);
    cos_t[i] = -sin(PI * i / 8);
    sin_t[i] = cos(PI * i / 8);
  }
  pinMode(LED, OUTPUT);
  pinMode(ENC_A, INPUT_PULLUP);
  pinMode(ENC_B, INPUT_PULLUP);
  DDRD = 0x00;
  PORTD = 0xFF;
  // なぜかPORTDでプルアップにできない
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);
//  Serial.begin(9600);
  Mouse.begin();
}

void loop()
{
  int sgn;
  unsigned int spd, ang;

  while (digitalRead(ENC_A) == HIGH) ;
  digitalWrite(LED, HIGH);
  sgn = digitalRead(ENC_B) ? -1 : 1;
  ang = ~PIND;
  spd = ang & 0x0F;
  ang = ang >> 4 & 0x0F;
//  Serial.println(String(sgn) + ',' + String(spd) + ',' + String(ang));
  Mouse.move(sgn * spd_t[spd] * cos_t[ang], sgn * spd_t[spd] * sin_t[ang]);
  delayMicroseconds(DELAY);

  while (digitalRead(ENC_A) == LOW) ;
  digitalWrite(LED, LOW);
  delayMicroseconds(DELAY);
}

