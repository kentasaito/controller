<script src="<?php site_url(); ?>js/jquery-1.12.2.min.js"></script>
<script>
var v = 0;

function move()
{
	$('#img').css({
		left:(v * 1 + 150),
		top:(v * 0.2 + 100),
		transform: 'rotate(' + ((v - 50) * 0.1) + 'deg)'
	});
}
$(function(){
	var size = 250;

	$('#img').attr({
		width:size,
		height:size
	}).css({
		width:size,
		height:size,
		overflow:'hidden',
		position:'absolute',
		top:100,
		left:0,
		transformOrigin:'center center'
	});
	$('#pad').mousemove(function(e){
		v = (e.pageY - $('#pad').position().top) / 7;
	});
	setInterval(move, 20);
});
</script>

<p>Move your mouse up and down.</p>
<div id="demo" style="position:relative;">
<div id="pad" style="position:absolute; background-color:#eee; width:700px; height:700px;">
<img id="img" src="<?php site_url(); ?>img/demo/sdchan2.svg">
</div>
