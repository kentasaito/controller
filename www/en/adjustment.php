<script src="<?php site_url(); ?>js/jquery-1.12.2.min.js"></script>
<script>
var ctx;
var t = 0;
var pos = {x:0, y:0};
var offset;

function flash()
{
	if (flash.last == undefined)
	{
		flash.last = {x:0, y:0};
	}
	$('#xvalue').text((pos.x - offset.x));
	$('#yvalue').text((pos.y - offset.y));
	var current = pos;
	if (t == 0)
	{
		ctx.clearRect(0, 0, $('#pad').attr('width'), $('#pad').attr('height'));
	}

	ctx.beginPath();
	ctx.moveTo(t, flash.last.y);
	ctx.lineTo(t + 1, current.y);
	ctx.stroke();

	flash.last.x = current.x;
	flash.last.y = current.y;
	t += 1;
	if (t >= $('#pad').attr('width'))
	{
		t = 0;
	}
}

$(function(){
	offset = {x:0, y:0};

	$('#xlabel, #ylabel, #xvalue, #yvalue').css({position:'absolute', fontSize:'50px', pointerEvents:'none'});
	$('#ylabel, #yvalue').css({top:'50px'});
	$('#xvalue, #yvalue').css({width:'160px', textAlign:'right'});
	$('#pad').css({backgroundColor:'#eee'});
	$('#pad').mousemove(function(e){
		pos.x = e.offsetX;
		pos.y = e.offsetY;
	});
	$('#pad').click(function(e){
		offset.x = e.offsetX;
		offset.y = e.offsetY;
		t = 0;
		flash();
	});
	ctx= $('#pad')[0].getContext('2d');
	ctx.lineWidth = 4;
	setInterval(flash, 10);
});
</script>
<p>Click to initialize offset position.</p>
<div id="adjustment" style="position:relative;">
<div id="xlabel">X:</div>
<div id="ylabel">Y:</div>
<div id="xvalue"></div>
<div id="yvalue"></div>
<canvas id="pad" width="700" height="700"></canvas>
</div>
