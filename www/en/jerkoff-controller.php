<div class="container">
<div class="row">
  <div class="col-md-4"><a href="<?php site_url(); ?>img/jerkoff/prototype.jpg"><img src="<?php site_url(); ?>img/jerkoff/prototype_t.jpg" style="width:100%;"></a></div>
  <div class="col-md-8">
    <h1>Kuroneko Jerkoff Controller</h1>
    <h2>Tension Input Device as USB HID Mouse</h2>
  </div>
</div>

<p><a href="<?php site_url(); ?>ja/jerkoff-controller">Japanese Page is Available</a></p>

<div class="alert alert-info" role="alert">This product is under development.(We're now waiting PCBA service)<br>We welcome the reservation.</div>

<h3>Features</h3>
<ul class="list-group">
<li class="list-group-item">USB Interface
	<ul>
	<li>USB 2.0 Full Speed</li>
	<li>Bus Power Drive</li>
	</ul>
</li>
<li class="list-group-item">Around 3.00N(0.306kgf) Tension Detection
	<ul>
	<li>Chattering Suppression at Lower/Upper Dead Center</li>
	<li>Sensing Posision Adjustable String</li>
	</ul>
</li>
<li class="list-group-item">Speed Controll
	<ul>
	<li>20 Pixels Each / 32 Levels / 0 to 640 Pixels</li>
	<li>With Hysteresis</li>
	</ul>
</li>
<li class="list-group-item">Angle Controll
	<ul>
	<li>15 Degrees Each / 24 Levels / 0 to 360 degrees</li>
	<li>With Hysteresis</li>
	</ul>
</li>
<li class="list-group-item">Mouse Left Button Hold(Drag) Emulation
	<ul>
	<li>With LED Indication</li>
	</ul>
</li>
</ul>
<h3>Demos</h3>
<p><a href="<?php site_url(); ?>en/adjustment">Adjustment</a></p>
<p><a href="<?php site_url(); ?>en/character">Character(NSFW)</a></p>
<h3>Videos</h3>
<p><a target="_blank" href="http://www.nicovideo.jp/watch/sm28412215">Jerk Off Controller #1</a></p>
<h3>Ordering</h3>
<p>Ask us directly:<br>
<a href="mailto:kuroneko.science@gmail.com">kuroneko.science@gmail.com</a></p>
</div>
