<div class="container">
<div class="row">
  <div class="col-md-4"><a href="<?php site_url(); ?>img/jerkoff/prototype.jpg"><img src="<?php site_url(); ?>img/jerkoff/prototype_t.jpg" style="width:100%;"></a></div>
  <div class="col-md-8">
    <h1>Blackcat Jerkoffコントローラ</h1>
    <h2>USB HIDマウスとしての張力入力デバイス</h2>
  </div>
</div>

<div class="alert alert-info" role="alert">この製品は開発中です。(外注基板の到着を待っています)<br>予約相談受付中です。</div>

<h3>特徴</h3>
<ul class="list-group">
<li class="list-group-item">USBインターフェイス
	<ul>
	<li>USB2.0フルスピード</li>
	<li>バスパワー駆動</li>
	</ul>
</li>
<li class="list-group-item">3.00N(0.306kgf)近傍での張力検出
	<ul>
	<li>下死点/上死点でのチャタリング抑制</li>
	<li>検出位置調整用コード</li>
	</ul>
</li>
<li class="list-group-item">速度調整
	<ul>
	<li>20ピクセルずつ / 32段階 / 0〜640ピクセル</li>
	<li>ヒステリシス付き</li>
	</ul>
</li>
<li class="list-group-item">角度調整
	<ul>
	<li>15°ずつ / 24段階 / 0〜360°</li>
	<li>ヒステリシス付き</li>
	</ul>
</li>
<li class="list-group-item">マウス左ボタンホールド(ドラッグ)エミュレーション
	<ul>
	<li>LED表示付き</li>
	</ul>
</li>
</ul>
<h3>デモ</h3>
<p><a href="<?php site_url(); ?>en/adjustment">調整用</a></p>
<p><a href="<?php site_url(); ?>en/character">キャラクター(※エロ注意)</a></p>
<h3>ビデオ</h3>
<p><a target="_blank" href="http://www.nicovideo.jp/watch/sm28412215">Jerk Off Controller #1</a></p>
<h3>発注</h3>
<p>直接ご相談ください:<br>
<a href="mailto:kuroneko.science@gmail.com">kuroneko.science@gmail.com</a></p>
</div>
