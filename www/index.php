<?php
function site_url()
{
	echo str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
}

function show_404()
{
	die('404 Not Found');
}

$path_info = isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO'], '/') : 'en/index';
if ( ! preg_match('/^[a-z0-9-\/]*$/', $path_info) OR ! file_exists($path_info.'.php'))
{
	show_404();
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../favicon.ico">
    <title>kuroneko.science</title>
    <link href="<?php site_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php site_url(); ?>css/jumbotron-narrow.css" rel="stylesheet">
    <link href="<?php site_url(); ?>css/default.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">
<a href="<?php site_url(); ?>"><img class="img-responsive" src="<?php site_url(); ?>img/kuroneko.science.svg" alt="kuroneko.science"></a>

      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation"><a href="<?php site_url(); ?>en/jerkoff-controller">Jerkoff Controller</a></li>
            <li role="presentation"><a href="<?php site_url(); ?>en/contact">Contact</a></li>
          </ul>
        </nav>
      </div>

<?php require_once($path_info.'.php'); ?>

      <footer class="footer">
        <p>kuroneko.science since 2016</p>
      </footer>

    </div> <!-- /container -->
  </body>
</html>
