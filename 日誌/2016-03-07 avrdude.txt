avrdude -c avrisp -p m328p -P com6 -b 19200 -U lfuse:r:-:i -v
avrdude -c usbasp -p m8                     -U lfuse:r:low_fuse_val.hex:h -U hfuse:r:high_fuse_val.hex:h
-c <programmer>            Specify programmer type.
-p <partno>                Required. Specify AVR device.
-P <port>                  Specify connection port.
-b <baudrate>              Override RS-232 baud rate.

low_fuses      = 0xff
high_fuses     = 0xd0
extended_fuses = 0xc8

unlock_bits    = 0x3F
lock_bits      = 0x2F


avrdude -c arduino -p m328p -P /dev/ttyACM0 -U lfuse:r:-:i
これでそれっぽいものが読めた。
:0100000000FF
:00000001FF


:0100000000FF
:00000001FF

