#include "Mouse.h"

#define UPPER 370
#define RANGE 64
#define LOWER (UPPER - RANGE + 1)

#define TENSION_SENSOR A6
#define SPEED_VR A7
#define ANGLE_VR A11
#define WARNING_LED 13
#define DRAG_LED 1

int lastTension = 0;
volatile int drag = 0;
volatile unsigned long lastMillis = 0;
int sx = 0;
int sy = 0;

#define BUF_SIZE 100
int buf[BUF_SIZE];
int buf_cnt = 0;

int average(int v)
{
  int sum = 0;
  int i;
  
  buf[buf_cnt] = v;
  for (i = 0; i < BUF_SIZE; i++)
  {
    sum += buf[i];
  }
  buf_cnt = (buf_cnt + 1) % BUF_SIZE;
  return floor(sum / BUF_SIZE);
}

void setup() {
  CLKPR = 0x80;
  CLKPR = 0x00;  
  attachInterrupt(0, dragSwDown, LOW);
  pinMode(WARNING_LED, OUTPUT);
  pinMode(DRAG_LED, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  int tension;
  int delta;
  double radius;
  double theta;
  int dx;
  int dy;
  
  tension = analogRead(TENSION_SENSOR);
  digitalWrite(WARNING_LED, tension >= LOWER && tension <= UPPER);
  tension = constrain(tension, LOWER, UPPER);
  delta = tension - lastTension;
  radius = analogRead(SPEED_VR) / 32.0 * delta;
  theta = (analogRead(ANGLE_VR) - 256.0) * PI / 512.0;
  dx = radius * cos(theta);
  dy = radius * sin(theta);
  sx += dx;
  sy += dy;
  if (tension >= UPPER)
  {
    if (sx != 0 || sy != 0)
    {
      dx -= sx;
      dy -= sy;
    }
    sx = 0;
    sy = 0;
  }
  while (dx > 127)
  {
    Mouse.move(127, 0);
    dx -= 127;
  }
  while (dx < -127)
  {
    Mouse.move(-127, 0);
    dx -= -127;
  }
  while (dy > 127)
  {
    Mouse.move(0, 127);
    dy -= 127;
  }
  while (dy < -127)
  {
    Mouse.move(0, -127);
    dy -= -127;
  }
  Mouse.move(dx, dy);
  lastTension = tension;
}

void dragSwDown()
{
  if (millis() < lastMillis + 50)
  {
    lastMillis = millis();
    return;
  }
  drag = ! drag;
  drag ? Mouse.press() : Mouse.release();
  digitalWrite(DRAG_LED, drag);
  lastMillis = millis();
}

