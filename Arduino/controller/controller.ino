#include <Mouse.h>
#include <MsTimer2.h>

#define TENSION_SENSOR A6
#define SPEED_VR A7
#define ANGLE_VR A11
#define WARNING_LED 13
#define DRAG_LED 1

#define OFFSET 500
#define RANGE 20
#define SAMPLING 20
#define HYSTERESIS 10

#define DEBUG 0

void clkpr()
{
  delay(1);
  CLKPR = 0x80;
  CLKPR = 0x00;
}

void greeting()
{
  for (int i = 0; i < 5; i++)
  {
    digitalWrite(1, HIGH);
    digitalWrite(13, HIGH);
    delay(50);
    digitalWrite(1, LOW);
    digitalWrite(13, LOW);
    delay(50);
  }
}

int hysteresis(int i) {
  static int current[] = {0, 0};
  const static int pin[] = {SPEED_VR, ANGLE_VR};
  const static float divisor[] = {31.03, 40.96};
  int raw = analogRead(pin[i]);
  int candidate = raw / divisor[i];

  if (candidate < current[i] && (raw + HYSTERESIS) / divisor[i] < current[i] || candidate > current[i] && (raw - HYSTERESIS) / divisor[i] > current[i])
  {
      current[i] = candidate;
  }
  return current[i];
}

double average(int v) {
  static int buf[SAMPLING];
  static int cnt = 0;
  static long sum = 0;

  sum -= buf[cnt];
  sum += buf[cnt] = v;
  cnt = (cnt + 1) % SAMPLING;
  return (float)sum / SAMPLING;
}

void flash() {
  static int x;
  static int y;
  int raw = analogRead(TENSION_SENSOR);
  int speed = hysteresis(0);
  int angle = hysteresis(1);
  double radius = average(constrain(OFFSET - raw, 0, RANGE)) * speed;
  int dx = min(max(radius * -sin(angle * PI / 12) - x, -127), 127);
  int dy = min(max(radius * cos(angle * PI / 12) - y, -127), 127);

  if (abs(dx) >= 1 || abs(dy) >= 1)
  {
    Mouse.move(dx, dy);
    x += dx;
    y += dy;
  }
  digitalWrite(WARNING_LED, raw < OFFSET);
  if (DEBUG)
  {
    Serial.print("    raw:"); Serial.print(raw);
    Serial.print(" radius:"); Serial.print(radius);
    Serial.print("      x:"); Serial.print(x);
    Serial.print("      y:"); Serial.print(y);
    Serial.print("  speed:"); Serial.print(speed * RANGE);
    Serial.print("  angle:"); Serial.print(angle * 15);
    Serial.println("");
  }
}

void dragSwDown()
{
  static unsigned long lastMillis = 0;
  static int drag;

  if (millis() < lastMillis + 50)
  {
    lastMillis = millis();
    return;
  }
  drag = ! drag;
  drag ? Mouse.press() : Mouse.release();
  digitalWrite(DRAG_LED, drag);
  lastMillis = millis();
}

void setup() {
  clkpr();
  pinMode(WARNING_LED, OUTPUT);
  pinMode(DRAG_LED, OUTPUT);
  if (DEBUG)
  {
    Serial.begin(115200);
  }
  greeting();
  MsTimer2::set(2, flash);
  MsTimer2::start();
  attachInterrupt(0, dragSwDown, LOW);
}

void loop() {
}

